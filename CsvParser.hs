{-# LANGUAGE OverloadedStrings #-}
module CsvParser (parseCsv, row, cell, quotedCell, simpleCell) where
import Prelude hiding (concat)
import Data.Text
import qualified Data.Text.Lazy as L
import Data.Attoparsec.Text.Lazy
import Types

parseCsv :: L.Text -> [Either String Row]
parseCsv "" = []
parseCsv str = case parse row str of
  -- TODO josh: work out how to give better error context on fails
  Fail _ _ err -> [Left err]
  Done rest v -> Right v : parseCsv rest
type P = Parser

row :: P Row
row = cell `sepBy` (char ',') <* choice [endOfInput, endOfLine]

cell :: P Cell
cell = (Cell . unpack) <$> (choice [quotedCell, simpleCell])

quotedCell :: P Text
quotedCell = (char '"') *> cellBody <* (char '"') where
  cellBody = concat <$> (many' $ choice [quote, notQuote])
  quote = const "\"" <$> string "\"\""
  notQuote = takeWhile1 $ \c -> c /= '"'


simpleCell :: P Text
simpleCell = takeTill delimiter where
  delimiter c = c == ',' || c == '\n'
