module Parser (
  module Text.Megaparsec,
  module Text.Megaparsec.Char,
  Parser,
  ErrorBundle,
) where
import Prelude ()
import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Void
import Data.Text.Lazy

type Parser = Parsec Void Text
type ErrorBundle = ParseErrorBundle Text Void
