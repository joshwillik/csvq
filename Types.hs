module Types where
newtype Column = Column { name :: String } deriving (Show, Eq)
newtype Cell = Cell { value :: String } deriving (Show, Eq)
type Row = [Cell]

