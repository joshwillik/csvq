{-# LANGUAGE QuasiQuotes #-}
module Main where
import Control.Monad
import Data.Either
import System.Exit
import System.IO
import qualified Data.Text.Lazy as Text
import qualified Data.Text.Lazy.IO as T
import System.Environment
import System.Console.Docopt
import Parser (runParser, Parser, ErrorBundle)
import ArgParser (cliArgs)
import CsvParser (parseCsv)
import CsvTransform
import Format

usageText :: Docopt
usageText = [docopt|
Usage:
csvq <columns> [<file>]
Options:
  <columns>  A selection of columns to use.
             By index: 1,2
             By name: field1,"complex name"
  <file>     The input file
  -r, --raw  Output in CSV mode (default is table mode)
|]

main :: IO ()
main = do
  args <- getArgs >>= parseArgsOrExit usageText
  let filename = getArg args $ argument "file"
  handle <- case filename of
            Just path -> openFile path ReadMode
            Nothing -> return stdin
  let fieldString = Text.pack $ getArgWithDefault args "" $ argument "columns"
  fields <- unwrap $ megaParse cliArgs fieldString
  body <- T.hGetContents handle
  let rows = parseCsv body
  when (null rows) exitSuccess
  headerRow <- case rows !! 0 of
               Right v -> return v
               Left e -> fail e
  let indexes = fieldIndexes fields headerRow
  let rewrite = extractFields indexes
  let previewRows = rights $ take 50 rows
  let outputMode = case (args `isPresent` longOption "raw") of
                   True -> Csv
                   False -> Table $ columnSizes $ map rewrite previewRows
  let printRow r = case r of
                   Right row -> putStrLn $ stringify outputMode $ rewrite row
                   Left err -> fail err -- TODO josh: better error context
  printRow $ head rows
  putStr $ headerDivider outputMode
  mapM_ printRow $ tail rows
  return ()

megaParse :: Parser a -> Text.Text -> Either ErrorBundle a
megaParse p input = runParser p "" input

unwrap :: Show e => Either e a -> IO a
unwrap v = case v of
  Left e -> fail $ show e
  Right v' -> return v'
