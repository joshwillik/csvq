module Util where

(!!?) :: [a] -> Int -> Maybe a
[] !!? 0 = Nothing
(x:_) !!? 0 = Just x
xs !!? n = (drop n xs) !!? 0
