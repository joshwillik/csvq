module CsvTransform where
import Text.Read (readMaybe)
import Data.List (elemIndex)
import Util
import Types

fieldIndexes :: [Column] -> [Cell] -> [Maybe Int]
fieldIndexes (c:columns) cells = found : fieldIndexes columns cells where
  found = findIndex (name c) $ map value cells
fieldIndexes [] _ = []

findIndex :: String -> [String] -> Maybe Int
findIndex x columns = case readMaybe x of
  Just v -> Just v
  Nothing -> x `elemIndex` columns

extractFields :: [Maybe Int] -> Row -> Row
extractFields (Nothing:xs) cells = Cell "" : extractFields xs cells
extractFields (Just x:xs) cells = extracted : extractFields xs cells where
  extracted = case cells !!? x of
    Nothing -> Cell ""
    Just v -> v
extractFields [] _ = []
