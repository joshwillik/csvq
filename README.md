# csvq

ABANDONED: I'm leaving this program in its current state, and I may add features later for purely interest reasons.

However, [dsq](https://github.com/multiprocessio/dsq) seems to be a much
smarter implementation of the same idea.

The original goal of this was something similar to
[jq](https://github.com/stedolan/jq) but for CSV files.

## Usage

```
$ csvq 0,2 file.csv
```

```
$ csvq name,age file.csv
```

## Planned features

- Filtering rows based on some conditions
- Transforming field values. Ideas:
  - `age|limit:100`
  - `limit(age, 100)`
