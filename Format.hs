module Format where
import Data.List
import Text.Printf
import Types

data OutputMode = Table [Int] | Csv deriving (Show)

headerDivider :: OutputMode -> String
headerDivider Csv = ""
headerDivider (Table widths) = printf "%s\n" line where
  line = unwords $ formatCells $ zip widths $ repeat $ repeat '-'

stringify :: OutputMode -> [Cell] -> String
stringify Csv cells = intercalate "," $ map value cells
stringify (Table widths) cells = unwords $ formatCells $ zip widths values where
  values = map value cells

formatCells :: [(Int, String)] -> [String]
formatCells ((width, cell):rest) = out : formatCells rest where
  out = printf "%-*s" width (take width cell)
formatCells [] = []

columnSizes :: [Row] -> [Int]
columnSizes rows = map maximum $ transpose lengths where
  lengths = map (map (length . value)) rows
