{-# LANGUAGE OverloadedStrings #-}
import Test.Hspec
import Data.Attoparsec.Text.Lazy
import qualified Data.Text.Lazy as T
import Parser
import CsvParser
import CsvTransform
import Types
import Format
import ArgParser

main :: IO ()
main = hspec $ do
  describe "csv parser" $ do
    let parse' p str = parseOnly p str
    it "simpleCell" $ do
      parse' simpleCell "hi,there" `shouldBe` Right "hi"
    it "quotedCell" $ do
      parse' quotedCell "\"I am 5'11\"\" tall\""
        `shouldBe` Right "I am 5'11\" tall"
    it "row" $ do
      parse' row "\"hi,hi\",there"
        `shouldBe` Right [Cell "hi,hi", Cell "there"]
    it "parseCsv" $ do
      let rawCsv = T.pack $ unlines
                   [
                     "name,age",
                     "\"smith, john\",12",
                     "jimmy john,23"
                   ]
      let parsed = map (Right . map Cell)
                       [
                         ["name", "age"],
                         ["smith, john", "12"],
                         ["jimmy john", "23"]
                       ]
      parseCsv rawCsv `shouldBe` parsed

  describe "arg parser" $ do
    let parse' p s = runParser p "" s
    it "simple" $ do
      parse' cliArgs "1,0,3"
        `shouldBe` Right [Column "1", Column "0", Column "3"]
    it "with quotes" $ do
      parse' cliArgs "simple,'testing',\"testing2\""
        `shouldBe` Right [
          Column "simple",
          Column "testing",
          Column "testing2"
        ]

  describe "fieldIndexes" $ do
    it "basic" $ do
      (fieldIndexes [Column "bar", Column "foo", Column "biz"]
        [Cell "foo", Cell "bar"]) `shouldBe` [Just 1, Just 0, Nothing]
  describe "findIndex" $ do
    it "numeric" $ do
      findIndex "0" [] `shouldBe` Just 0
    it "existing text" $ do
      findIndex "bar" ["foo", "bar"] `shouldBe` Just 1
    it "missing text" $ do
      findIndex "biz" ["foo", "bar"] `shouldBe` Nothing
  describe "extractFields" $ do
    it "basic" $ do
      extractFields (map Just [0, 2]) (map Cell ["foo", "bar", "biz"])
        `shouldBe` [Cell "foo", Cell "biz"]
    it "missing fields" $ do
      extractFields (map Just [0, 1]) (map Cell ["foo"])
        `shouldBe` [Cell "foo", Cell ""]

  describe "format" $ do
    let cells = map Cell ["hi", "", "there"]
    let nocells = map Cell []
    it "stringify csv" $ do
      stringify Csv cells `shouldBe` "hi,,there"
    it "stringify csv" $ do
      stringify Csv nocells `shouldBe` ""
    it "stringify table" $ do
      stringify (Table [2, 2, 10]) cells
        `shouldBe` unwords ["hi", "  ", "there     "]
    it "stringify nocells table" $ do
      stringify (Table []) nocells `shouldBe` ""
    it "stringify overflow table" $ do
      stringify (Table [1, 1, 1])  cells `shouldBe` "h   t"
