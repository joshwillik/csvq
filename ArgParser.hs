module ArgParser where
import Parser
import Types

type P = Parser

cliArgs :: P [Column]
cliArgs = column `sepBy` (char ',')

column :: P Column
column = pure Column <*> (wrappedField '"' <|> wrappedField '\''
  <|> simpleField)

wrappedField :: Char -> P String
wrappedField c = between (char c) (char c) $ many $ noneOf [c]

simpleField :: P String
simpleField = many $ noneOf [',', '\n']
