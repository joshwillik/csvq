- Support better error messages (with context) on bad csv rows or bad user
  input or whatever
- Add support for column filters
  ? '1,2|slice:50' # simple filter functions
  ? '1,slice(2, 50)' # real expression language
  ? '1,2|cut -c1,80)' # output to shell commands for transforms
- Remove docopt, the current docopt haskell lib is too buggy
- Support loading the CSV in interactive mode and live transforming the columns
  (Maybe this is a new project)
